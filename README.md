# gitlab-storage-cleanup

Gitlab recently started enforcing [comically small storage quotas](https://news.ycombinator.com/item?id=32386323) on projects and namespaces. They don't, however, have a good way to cleanup old CI artifacts from projects at a namespace level. This project iterates through projects and deletes old artifacts based on the age threshold you set.

## Usage

Checkout and install dependencies:

```bash
git clone git@gitlab.com:thelabnyc/gitlab-storage-cleanup.git
cd gitlab-storage-cleanup
poetry install
```

Show help:

```bash
poetry run gitlab-storage-cleanup --help
```

Delete all artifacts older than 14 days for the project `my-group/myproject`.

```bash
GITLAB_ACCESS_TOKEN='foo-bar-baz' poetry run gitlab-storage-cleanup '^my-group\/myproject$'
```

Delete all artifacts older than 30 days for all projects in under the `my-group` namespace.

```bash
GITLAB_ACCESS_TOKEN='foo-bar-baz' poetry run gitlab-storage-cleanup --min-days-old 30 '^my-group\/'
```

## Help

```bash
usage: gitlab-storage-cleanup [-h] [-d MIN_DAYS_OLD] [-w WORKERS] [-H HOSTNAME] [-a] [-D] project_path_pattern

Delete old artifacts from Gitlab to reduce storage usage

positional arguments:
  project_path_pattern  Regex pattern controlling which projects to cleanup. E.g. '^my-group\/'

options:
  -h, --help            show this help message and exit
  -d MIN_DAYS_OLD, --min-days-old MIN_DAYS_OLD
                        Don't delete anything created within the last N days. Default is 14.
  -w WORKERS, --workers WORKERS
                        Number of worker threads to use. Default is 10.
  -H HOSTNAME, --hostname HOSTNAME
                        Gitlab API hostname. Default is 'gitlab.com'.
  -a, --all-projects    Delete artifacts for all projects, not only limited by the owner.
  -D, --dry-run         Don't actually delete anything. Just print what would be deleted.
```

## Disclaimer

This script can't read your mind—it may delete artifacts you want or need. Audit the code before use and use it at your own risk.

**Use dry-run mode, and check the output, before running it for real.**
